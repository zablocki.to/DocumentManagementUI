import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { Client } from '../client';
import { ClientService } from '../client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-client',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './add-client.component.html',
  styleUrl: './add-client.component.css'
})
export class AddClientComponent {
  readonly clientService: ClientService = inject(ClientService);
  readonly router: Router = inject(Router);
  photo: File | undefined = undefined;
  client: Client | undefined = undefined;

  uploadPhoto(event: any){
    this.photo = event.target.files[0];
  }

  processClientForm(form: NgForm, client: Client, photoInput: HTMLInputElement){
    if (this.client){
      client.id = this.client.id;
      console.log(`Update client: ${client}`);
      this.clientService.saveClient(client, this.photo).subscribe(
        (client: Client | undefined) => {
          if (client){
            this.router.navigate(['/client-details', client.id]);
          }
        },
        (error) => {
          console.error("Problem to save client: ", error);
        }
      );
    } else {
      console.log(`Add Client: ${client}`);
      let result: Observable<Client> | undefined = this.clientService.saveClient(client, this.photo);
      this.logResult(result);
    }
    form.reset();
    photoInput.value = '';
    this.photo = undefined;
    this.client = undefined;
  }

  logResult(result: Observable<Client>){
      result.subscribe(
                (savedClient: Client) => {
                   console.log("Client saved: ", savedClient);
                },
                (error) => {
                  console.error("Problem with saving client: ", error);
                }
             );
  }

  constructor(private route: ActivatedRoute){
    const clientId = route.snapshot.paramMap.get('id');
    this.clientService.getClientById(clientId).subscribe(
      (client: Client | undefined) => {
        this.client = client;
      },
      (error) => {
        console.error("Problem to get client:", error);
      }
    );
  }
}
