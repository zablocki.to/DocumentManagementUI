export interface PageResultObject<T> {
  totalElements: number,
  totalPages: number,
  elements: T[]
}
