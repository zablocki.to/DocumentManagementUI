import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentService } from '../document.service';
import { inject } from '@angular/core';
import { Client } from '../client';
import { ClientService } from '../client.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Document, AssignedEntity } from '../document';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-client-details',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './client-details.component.html',
  styleUrl: './client-details.component.css'
})
export class ClientDetailsComponent {
  readonly documentService: DocumentService = inject(DocumentService);
  readonly clientService: ClientService = inject(ClientService);
  readonly router: Router = inject(Router);
  client: Client = {
                         id: '',
                         gender: 'M',
                         photoPath: '',
                         firstName: '',
                         lastName: '',
                         streetAddress: '',
                         postCodeAddress: '',
                         cityAddress: ''
                       };
  clientPhotoPath: string = '/assets/client_man.png';
  documentNames: Set<string> = new Set<string>();
  filesToUpload: File[] = [];

  chooseFiles(event: any){
    this.filesToUpload = event.target.files;
  }

  deleteClient(id: string) {
    this.clientService.deleteClientById(id).subscribe(
      (client: Client | undefined) => {
        console.info("Client deleted: ", client);
        this.router.navigate(['/clients']);
      },
      (error) => {
        console.error(`Could not delete Client with id: ${id} `, error);
      }
    );
  }

  saveDocument(fileInput: HTMLInputElement){
    const addingFileResponses: Observable<Document>[] = this.documentService.saveDocument(this.filesToUpload, 'CLIENT', this.client.id);
    for(let response of addingFileResponses){
      response.subscribe(
                    response => {
                      console.info("Successfully saved:", response);
                      this.documentNames.add(response.name);
                    },
                    error => {
                      console.error("Saving failed:", error);
                    }
      );
    }
    this.filesToUpload = [];
    fileInput.value = '';
  }

  loadFile(documentName: string){
    console.info("Uploading file:", documentName);
    this.documentService.loadDocument(documentName, this.client.id, 'CLIENT').subscribe(
      (doc: Document) => {
          const fileUrl = doc.filePath;
          console.info("Download file from link:", fileUrl);
          const downloadLink = document.createElement('a');
          downloadLink.href = fileUrl;
          downloadLink.setAttribute('download', documentName);
          document.body.appendChild(downloadLink);
          downloadLink.click();
          document.body.removeChild(downloadLink);
      },
      (error) => {
        if (error.status === 404){
          console.error(`File with name ${documentName} not found!`);
        }
        console.error("Problem to get document:", error);
      }
    );
  }

  removeFile(documentName: string) {
    console.info("Removing file: ", documentName);
    this.documentService.deleteDocument(documentName, this.client.id, 'CLIENT').subscribe(
      (doc: Document) => {
        console.info("Document removed: ", doc);
        this.documentNames.delete(doc.name);
      },
      (error) => {
        console.error("Problem to remove document: ", error);
      }
    );
  }

  constructor(private route: ActivatedRoute){
    const clientId = route.snapshot.paramMap.get('id');

    const getClientPhotoPath = (client: Client) => {
          if (client.photoPath){
            return client.photoPath;
          }
          return client.gender==='M' ? '/assets/client_man.png' : '/assets/client_woman.png';
    };

    this.clientService.getClientById(clientId).subscribe(
       (client: Client | undefined) => {
         if (client !== undefined){
           this.client = client;
           this.clientPhotoPath = getClientPhotoPath(client);
         }
       },
       (error) => {
         console.error("Problem to get client:", error);
       }
    );

    this.documentService.loadDocumentNames(clientId, 'EMPLOYEE')
    .subscribe(
      (documentNames: Set<string>) => {
        this.documentNames = new Set(documentNames);
      },
      (error) => {
        console.error("Problem to get document names:", error);
      }
    );
  }
}
