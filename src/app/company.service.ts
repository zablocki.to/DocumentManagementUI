import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Company } from './company';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  readonly httpClient: HttpClient = inject(HttpClient);
  readonly serverUrl = "http://localhost:8080/companies";

  getAll(): Observable<Company[]>{
    return this.httpClient.get<Company[]>(this.serverUrl);
  }
  getCompanyById(id: string | null): Observable<Company | undefined>{
    return this.httpClient.get<Company | undefined>(this.serverUrl+`/${id}`);
  }
  saveCompany(company: Company): Observable<Company>{
    return this.httpClient.post<Company>(this.serverUrl, company);
  }
}
