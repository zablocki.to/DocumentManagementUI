import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentService } from '../document.service';
import { inject } from '@angular/core';
import { Company } from '../company';
import { CompanyService } from '../company.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Document, AssignedEntity } from '../document';

@Component({
  selector: 'app-company-details',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './company-details.component.html',
  styleUrl: './company-details.component.css'
})
export class CompanyDetailsComponent {
  readonly documentService: DocumentService = inject(DocumentService);
  readonly companyService: CompanyService = inject(CompanyService);
  company: Company = {
                         id: '',
                         name: '',
                         streetAddress: '',
                         postCodeAddress: '',
                         cityAddress: ''
                       };
  documentNames: Set<string> = new Set<string>();
  filesToUpload: File[] = [];

  chooseFiles(event: any){
    this.filesToUpload = event.target.files;
  }

  saveDocument(fileInput: HTMLInputElement){
    const addingFileResponses: Observable<Document>[] = this.documentService.saveDocument(this.filesToUpload, 'EMPLOYEE', this.company.id);
    for(let response of addingFileResponses){
      response.subscribe(
                    response => {
                      console.info("Successfully saved:", response);
                      this.documentNames.add(response.name);
                    },
                    error => {
                      console.error("Saving failed:", error);
                    }
      );
    }
    this.filesToUpload = [];
    fileInput.value = '';
  }

  loadFile(documentName: string){
    console.info("Uploading file:", documentName);
    this.documentService.loadDocument(documentName, this.company.id, 'EMPLOYEE').subscribe(
      (doc: Document | undefined) => {
        if (doc !== undefined){
          const fileUrl = doc.filePath;
          const downloadLink = document.createElement('a');
          downloadLink.href = fileUrl;
          downloadLink.setAttribute('download', documentName);
          document.body.appendChild(downloadLink);
          downloadLink.click();
          document.body.removeChild(downloadLink);
        } else {
          console.error("Not found document with name:", documentName);
        }
      },
      (error) => {
        console.error("Problem to get document:", error);
      }
    );
  }

  constructor(private route: ActivatedRoute){
    const companyId = route.snapshot.paramMap.get('id');
    this.companyService.getCompanyById(companyId).subscribe(
       (company: Company | undefined) => {
         if (company !== undefined){
           this.company = company;
         }
       },
       (error) => {
         console.error("Problem to get company:", error);
       }
    );

    this.documentService.loadDocumentNames(companyId, 'EMPLOYEE').subscribe(
      (documentNames: Set<string>) => {
        this.documentNames = new Set(documentNames);
      },
      (error) => {
        console.error("Problem to get document names:", error);
      }
    );
  }
}
