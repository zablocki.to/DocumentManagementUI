import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentService } from '../document.service';
import { inject } from '@angular/core';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Document, AssignedEntity } from '../document';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-employee-details',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './employee-details.component.html',
  styleUrl: './employee-details.component.css'
})
export class EmployeeDetailsComponent {
  readonly documentService: DocumentService = inject(DocumentService);
  readonly employeeService: EmployeeService = inject(EmployeeService);
  readonly router: Router = inject(Router);
  employee: Employee = {
                         id: '',
                         gender: 'M',
                         photoPath: '',
                         firstName: '',
                         lastName: '',
                         department: ''
                       };
  employeePhotoPath: string = '/assets/employee_man.png';
  documentNames: Set<string> = new Set<string>();
  filesToUpload: File[] = [];

  chooseFiles(event: any){
    this.filesToUpload = event.target.files;
  }

  deleteEmployee(id: string) {
    this.employeeService.deleteEmployeeById(id).subscribe(
      (employee: Employee | undefined) => {
        console.info("Employee deleted: ", employee);
        this.router.navigate(['/employees']);
      },
      (error) => {
        console.error(`Could not delete Employee with id: ${id} `, error);
      }
    );
  }

  saveDocument(fileInput: HTMLInputElement){
    const addingFileResponses: Observable<Document>[] = this.documentService.saveDocument(this.filesToUpload, 'EMPLOYEE', this.employee.id);
    for(let response of addingFileResponses){
      response.subscribe(
                    response => {
                      console.info("Successfully saved:", response);
                      this.documentNames.add(response.name);
                    },
                    error => {
                      console.error("Saving failed:", error);
                    }
      );
    }
    this.filesToUpload = [];
    fileInput.value = '';
  }

  loadFile(documentName: string) {
    console.info("Uploading file: ", documentName);
    this.documentService.loadDocument(documentName, this.employee.id, 'EMPLOYEE').subscribe(
      (doc: Document | undefined) => {
          const fileUrl = doc.filePath;
          console.info("Download file from link:", fileUrl);
          const downloadLink = document.createElement('a');
          downloadLink.href = fileUrl;
          downloadLink.setAttribute('download', documentName);
          document.body.appendChild(downloadLink);
          downloadLink.click();
          document.body.removeChild(downloadLink);
      },
      (error) => {
        if (error.status === 404){
          console.error(`File with name ${documentName} not found!`);
        }
        console.error("Problem to get document: ", error);
      }
    );
  }

  removeFile(documentName: string) {
    console.info("Removing file: ", documentName);
    this.documentService.deleteDocument(documentName, this.employee.id, 'EMPLOYEE').subscribe(
      (doc: Document) => {
        console.info("Document removed: ", doc);
        this.documentNames.delete(doc.name);
      },
      (error) => {
        console.error("Problem to remove document: ", error);
      }
    );
  }

  constructor(private route: ActivatedRoute){
    const employeeId = route.snapshot.paramMap.get('id');

    const getEmployeePhotoPath = (employee: Employee) => {
      if (employee.photoPath){
        return employee.photoPath;
      }
      return employee.gender==='M' ? '/assets/employee_man.png' : '/assets/employee_woman.png';
    };

    this.employeeService.getEmployeeById(employeeId).subscribe(
       (employee: Employee | undefined) => {
         if (employee !== undefined){
           this.employee = employee;
           this.employeePhotoPath = getEmployeePhotoPath(employee);
         }
       },
       (error) => {
         console.error("Problem to get employee:", error);
       }
    );

    this.documentService.loadDocumentNames(employeeId, 'EMPLOYEE').subscribe(
      (employeeNames: Set<string>) => {
        this.documentNames = new Set(employeeNames);
      },
      (error) => {
        console.error("Problem to get document names:", error);
      }
    );
  }
}
