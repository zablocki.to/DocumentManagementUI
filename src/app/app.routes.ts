import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';

import { ClientsComponent } from './clients/clients.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { AddClientComponent } from './add-client/add-client.component';

import { CompaniesComponent } from './companies/companies.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { AddCompanyComponent } from './add-company/add-company.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    title: 'Home page'
  },

  {
    path: 'employees',
    component: EmployeesComponent,
    title: 'Employees page'
  },
  {
    path: 'add-employee/:id',
    component: AddEmployeeComponent,
    title: 'Adding new Employee'
  },
  {
    path: 'employee-details/:id',
    component: EmployeeDetailsComponent,
    title: 'Detail Documents for Employee'
  },
  {
    path: 'clients',
    component: ClientsComponent,
    title: 'Clients page'
  },
  {
    path: 'add-client/:id',
    component: AddClientComponent,
    title: 'Adding new Client'
  },
  {
    path: 'client-details/:id',
    component: ClientDetailsComponent,
    title: 'Detail Documents for Client'
  },
  {
    path: 'companies',
    component: CompaniesComponent,
    title: 'Companies page'
  },
  {
    path: 'add-company',
    component: AddCompanyComponent,
    title: 'Adding new Company'
  },
  {
    path: 'company-details/:id',
    component: CompanyDetailsComponent,
    title: 'Detail Documents for Company'
  }
];
