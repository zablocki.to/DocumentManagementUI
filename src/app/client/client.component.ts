import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Client } from '../client';

@Component({
  selector: 'app-client',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './client.component.html',
  styleUrl: './client.component.css'
})
export class ClientComponent {
  @Input() client!: Client;
  clientPhotoPath: string = '/assets/client_man.png';

  ngOnInit(): void {
    const getClientPhotoPath = (client: Client) => {
       if (client.photoPath){
          return client.photoPath;
       }
          return client.gender==='M' ? '/assets/client_man.png' : '/assets/client_woman.png';
    };
    this.clientPhotoPath = getClientPhotoPath(this.client);
  }
}
