import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from './employee';
import { PageResultObject } from './page-result-object';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  readonly httpClient: HttpClient = inject(HttpClient);
  readonly serverUrl = "http://localhost:8080/employees";

  getAll(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.serverUrl);
  }

  getPaged(pageNum: number, pageSize: number, nameFilter: string = ""): Observable<PageResultObject<Employee>> {
    return this.httpClient.get<PageResultObject<Employee>>(this.serverUrl + `?pageNum=${pageNum}&pageSize=${pageSize}&nameFilter=${nameFilter}`);
  }

  getEmployeeById(id: string | null): Observable<Employee | undefined> {
    return this.httpClient.get<Employee | undefined>(this.serverUrl+`/${id}`);
  }

  deleteEmployeeById(id: string): Observable<Employee | undefined> {
    return this.httpClient.delete<Employee | undefined>(this.serverUrl + `/${id}`)
  }

  saveEmployee(employee: Employee, photo: File | undefined): Observable<Employee>{
  //TODO add saving without photo separatly on other url!!!
    const formData: FormData = this.createFormWithPhotoAndEmployeeDescription(employee, photo);
    return this.httpClient.post<Employee>(this.serverUrl, formData);
  }

  createFormWithPhotoAndEmployeeDescription(employee: Employee, photo: File | undefined){
    const formData: FormData = new FormData();
    if (photo){
      formData.append('photo', photo, photo.name);
    }
    formData.append('employee', JSON.stringify(employee));
    return formData;
  }
}
