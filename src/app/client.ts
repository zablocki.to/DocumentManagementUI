export type Gender = 'M' | 'W';
export interface Client {
    id: string,
    gender: Gender,
    photoPath: string,
    firstName: string,
    lastName: string,
    streetAddress: string,
    postCodeAddress: string,
    cityAddress: string
}
