import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from './client';
import { PageResultObject } from './page-result-object';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  readonly httpClient: HttpClient = inject(HttpClient);
  readonly serverUrl = "http://localhost:8080/clients";

  getAll(): Observable<Client[]>{
    return this.httpClient.get<Client[]>(this.serverUrl);
  }

  getPaged(pageNum: number, pageSize: number, nameFilter: string = ""): Observable<PageResultObject<Client>> {
    return this.httpClient.get<PageResultObject<Client>>(this.serverUrl + `?pageNum=${pageNum}&pageSize=${pageSize}&nameFilter=${nameFilter}`);
  }

  getClientById(id: string | null): Observable<Client | undefined>{
    return this.httpClient.get<Client | undefined>(this.serverUrl+`/${id}`);
  }

  deleteClientById(id: string | null): Observable<Client | undefined> {
    return this.httpClient.delete<Client | undefined>(this.serverUrl + `/${id}`)
  }

  saveClient(client: Client, photo: File | undefined): Observable<Client>{
    //TODO add saving without photo separatly on other url!!!

    const formData: FormData = this.createFormWithPhotoAndClientDescription(client, photo);
    return this.httpClient.post<Client>(this.serverUrl, formData);
  }

  createFormWithPhotoAndClientDescription(client: Client, photo: File | undefined){
    const formData: FormData = new FormData();
    if (photo){
      formData.append('photo', photo, photo.name);
    }
    formData.append('client', JSON.stringify(client));
    return formData;
  }
}
