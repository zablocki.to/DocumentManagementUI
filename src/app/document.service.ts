import { Injectable, inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Document, AssignedEntity } from './document';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  readonly httpClient: HttpClient = inject(HttpClient);
  readonly serverUrl = "http://localhost:8080/document";

  loadDocument(documentName: string, assignedEntityId: string | null, assignedEntity: AssignedEntity): Observable<Document>{
    return this.httpClient.get<Document>(this.serverUrl + `/${documentName}/${assignedEntity}/${assignedEntityId}`);
  }

  loadDocumentNames(assignedEntityId: string | null, assignedEntity: AssignedEntity): Observable<Set<string>>{
    return this.httpClient.get<Set<string>>(this.serverUrl + `/document-names/${assignedEntity}/${assignedEntityId}`);
  }

  saveDocument(files: File[], assignedEntity: AssignedEntity, assignedEntityId: string): Observable<Document>[]{
    let addingFileResponses: Observable<Document>[] = []
    if (files){
      for(let f of files){
        console.log('Saving file:', f);
        const formData = this.createFormWithFileAndDocument(f, assignedEntity, assignedEntityId);
        const response: Observable<Document> = this.httpClient.post<Document>(this.serverUrl, formData);
        addingFileResponses.push(response);
      }
    }
    return addingFileResponses;
  }

  deleteDocument(documentName: string, assignedEntityId: string | null, assignedEntity: AssignedEntity): Observable<Document>{
    return this.httpClient.delete<Document>(this.serverUrl + `/${documentName}/${assignedEntity}/${assignedEntityId}`);
  }

  createFormWithFileAndDocument(file: File,
                                assignedEntity: AssignedEntity,
                                assignedEntityId: string): FormData {
    const formData: FormData = new FormData();
    const doc: Document = {
      id: file.name+assignedEntity+assignedEntityId,
      name: file.name,
      assignedEntity: assignedEntity,
      assignedEntityId: assignedEntityId,
      filePath: ''
    };
    console.log(file);
    formData.append('file', file, file.name);
    formData.append('document', JSON.stringify(doc));
    console.log(formData);
    return formData;
  }
}
