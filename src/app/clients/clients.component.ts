import { Component,inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientService } from '../client.service';
import { Client } from '../client';
import { ClientComponent } from '../client/client.component';
import { RouterModule } from '@angular/router';
import { PageResultObject } from '../page-result-object';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-clients',
  standalone: true,
  imports: [CommonModule, ClientComponent, RouterModule, FormsModule],
  templateUrl: './clients.component.html',
  styleUrl: './clients.component.css'
})
export class ClientsComponent {
  FIRST_PAGE = 1;
  readonly clientService: ClientService = inject(ClientService);
  filteredClients: Client[] = [];
  clients: Client[] = [];
  DEFAULT_PAGE_SIZE: number = 9;
  pageSize: number = this.DEFAULT_PAGE_SIZE;
  pagesNumbers: number[] = [];
  currentPageNumber: number = this.FIRST_PAGE;

  filterClients(nameFilter: string, pageNum: number = this.FIRST_PAGE){
    const REDUCE_TO_ZERO_START = 1;
    const pageNumber: number = pageNum - REDUCE_TO_ZERO_START;
    this.clientService.getPaged(pageNumber, this.pageSize, nameFilter).subscribe(
      (result: PageResultObject<Client>) => {
        this.pagesNumbers = Array.from({length: result.totalPages}, (_, i) => i+1);
        this.filteredClients = result.elements;
        this.currentPageNumber = pageNum;
      },
      (error) => console.error("Problem to get clients: ", error)
    );
  }

  constructor(){
    this.clientService.getPaged(0, this.pageSize, "").subscribe(
    (pagedResult: PageResultObject<Client>) => {
      this.filteredClients = pagedResult.elements;
      this.clients = pagedResult.elements;
      this.pagesNumbers = Array.from({length: pagedResult.totalPages}, (_, i) => i+1);
    },
    (error) => {
      console.error("Problem to get clients:", error);
    });
  }
}
