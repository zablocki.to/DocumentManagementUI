export type Gender = 'M' | 'W';
export interface Employee {
  id: string,
  gender: Gender,
  photoPath: string,
  firstName: string,
  lastName: string,
  department: string
}
