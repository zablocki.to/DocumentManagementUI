export interface Company {
    id: string,
    name: string,
    streetAddress: string,
    postCodeAddress: string,
    cityAddress: string
}
