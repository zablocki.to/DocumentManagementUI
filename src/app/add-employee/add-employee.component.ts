import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-employee',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './add-employee.component.html',
  styleUrl: './add-employee.component.css'
})
export class AddEmployeeComponent {
  readonly employeeService: EmployeeService = inject(EmployeeService);
  readonly router: Router = inject(Router);
  photo: File | undefined = undefined;
  employee: Employee | undefined = undefined;

  uploadPhoto(event: any){
    this.photo = event.target.files[0];
  }

  processEmployeeForm(form: NgForm, employee: Employee, photoInput: HTMLInputElement){
    if (this.employee){
      employee.id = this.employee.id;
      console.log(`Update employee: ${employee}`);
      this.employeeService.saveEmployee(employee, this.photo).subscribe(
        (employee: Employee | undefined) => {
          if(employee) {
            this.router.navigate(['/employee-details', employee.id]);
          }
        },
        (error) => {
          console.error("Problem to save employee: ", error);
        }
      );
    } else {
      console.log(`Save employee: ${employee}`)
      let result: Observable<Employee> | undefined = this.employeeService.saveEmployee(employee, this.photo);
      this.logResult(result);
    }

    form.reset();
    photoInput.value = '';
    this.photo = undefined;
    this.employee = undefined;
  }

  logResult(result: Observable<Employee>){
    result.subscribe(
              (employee: Employee) => {
                 console.info("Employee saved: ", employee);
              },
              (error) => {
                console.error("Problem with saving employee: ", error);
              }
          );
  }

  constructor(private route: ActivatedRoute){//injection through constructor is same as inject(ActivatedRoute) anywhere like in spring
    const employeeId = route.snapshot.paramMap.get('id');
    this.employeeService.getEmployeeById(employeeId).subscribe(
      (employee: Employee | undefined) => {
        this.employee = employee;
      },
      (error) => {
        console.error("Problem to get employee:", error);
      }
    );
  }
}
