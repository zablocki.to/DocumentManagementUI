import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { Company } from '../company';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-add-company',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './add-company.component.html',
  styleUrl: './add-company.component.css'
})
export class AddCompanyComponent {
  readonly companyService: CompanyService = inject(CompanyService);

  processCompanyForm(form: NgForm, company: Company){
    this.companyService.saveCompany(company).subscribe(
    (savedCompany: Company) => {
       console.log("Company saved: ", savedCompany);
    },
    (error) => {
      console.error("Problem with saving company: ", error);
    }
    );
    form.reset();
  }
}
