import { Component,inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';
import { EmployeeComponent } from '../employee/employee.component';
import { RouterModule } from '@angular/router';
import { PageResultObject } from '../page-result-object';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-employees',
  standalone: true,
  imports: [CommonModule, EmployeeComponent, RouterModule, FormsModule],
  templateUrl: './employees.component.html',
  styleUrl: './employees.component.css'
})
export class EmployeesComponent {
  FIRST_PAGE = 1;
  readonly employeeService: EmployeeService = inject(EmployeeService);
  filteredEmployees: Employee[] = [];
  employees: Employee[] = [];
  DEFAULT_PAGE_SIZE: number = 9;
  pageSize: number = this.DEFAULT_PAGE_SIZE;
  pagesNumbers: number[] = [];
  currentPageNumber: number = this.FIRST_PAGE;

  filterEmployees(nameFilter: string, pageNum: number = this.FIRST_PAGE){
    const REDUCE_TO_ZERO_START = 1;
    const pageNumber: number = pageNum - REDUCE_TO_ZERO_START;
    this.employeeService.getPaged(pageNumber, this.pageSize, nameFilter).subscribe(
      (result: PageResultObject<Employee>) => {
        this.pagesNumbers = Array.from({length: result.totalPages}, (_, i) => i+1);
        this.filteredEmployees = result.elements;
        this.currentPageNumber = pageNum;
      },
      (error) => console.error("Problem to get clients: ", error)
    );
  }

  constructor(){
    this.employeeService.getPaged(0, this.pageSize, "").subscribe(
    (pagedResult: PageResultObject<Employee>) => {
      this.filteredEmployees = pagedResult.elements;
      this.employees = pagedResult.elements;
      this.pagesNumbers = Array.from({length: pagedResult.totalPages}, (_, i) => i+1);
    },
    (error) => {
      console.error("Problem to get employees:", error);
    });
  }
}
