
export type AssignedEntity = 'CLIENT' | 'COMPANY' | 'EMPLOYEE';

export interface Document {
  id: string,
  name: string,
  assignedEntity: AssignedEntity,
  assignedEntityId: string,
  filePath: string
}
