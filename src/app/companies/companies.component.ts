import { Component,inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyService } from '../company.service';
import { Company } from '../company';
import { CompanyComponent } from '../company/company.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-companies',
  standalone: true,
  imports: [CommonModule, CompanyComponent, RouterModule],
  templateUrl: './companies.component.html',
  styleUrl: './companies.component.css'
})
export class CompaniesComponent {
  readonly companyService: CompanyService = inject(CompanyService);
  filteredCompanies: Company[] = [];
  companies: Company[] = [];

  constructor(){
    this.companyService.getAll().subscribe(
    (companies: Company[]) => {
      this.filteredCompanies = companies;
      this.companies = companies;
    },
    (error) => {
      console.error("Problem to get companies:", error);
    });
  }

  filterCompanies(nameFilter: string){
    this.filteredCompanies = this.companies.filter(c => c.name.toLowerCase().includes(nameFilter.toLowerCase()));
  }
}
