import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Employee } from '../employee';

@Component({
  selector: 'app-employee',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './employee.component.html',
  styleUrl: './employee.component.css'
})
export class EmployeeComponent {
  @Input() employee!: Employee;
  employeePhotoPath: string = '/assets/employee_man.png';

  ngOnInit(): void {
    const getEmployeePhotoPath = (employee: Employee) => {
        if (employee.photoPath){
           return employee.photoPath;
        }
           return employee.gender==='M' ? '/assets/employee_man.png' : '/assets/employee_woman.png';
    };
    this.employeePhotoPath = getEmployeePhotoPath(this.employee);
  }
}
